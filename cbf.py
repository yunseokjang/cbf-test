# -*- coding:utf-8 -*-

"""
cbf.py

do collaborative filtering with given matrix

"""
import numpy as np
from cos_similarity import ComputeCosineSimilarity


class CollaborativeFiltering(object):
    """ do collaborative filtering with given matrix
    """
    @classmethod
    def run(cls, train_matrix, test_data_matrix):
        """
        run collaborative filtering

        Args:
            train_matrix (numpy.array): user*item matrix with given ratings. has -0.5, +0.5 depends on their preferences
            test_data_matrix (any list of list): n by 3 test case list of list that want to guess

        Returns:
            numpy.array: m*m symmetric cosine similarity matrix
        """

        average_matrix, similarity_matrix = ComputeCosineSimilarity.run_zero_min(train_matrix)
        similarity_matrix[np.diag_indices_from(similarity_matrix)] = 0
        mean_subtracted_matrix = train_matrix - average_matrix[:, np.newaxis]

        error = 0.0
        for row in test_data_matrix:
            modular_value = np.absolute(similarity_matrix[row[0], np.nonzero(train_matrix[:, row[1]])]).sum()
            if modular_value != 0:
                variation = np.dot(mean_subtracted_matrix[:, row[1]],
                                   similarity_matrix[row[0], :] * (train_matrix[:, row[1]] != 0)) / modular_value
            else:
                variation = 0
            diff = row[2] - (average_matrix[row[0]] + variation)
            error += diff * diff

        return np.sqrt(error / len(test_data_matrix))
