# -*- coding:utf-8 -*-

"""
matrix_builder.py

build user-item matrix

"""
import numpy as np


class BuildMatrix(object):
    """ compute cosine similarity of row vectors in numpy matrix
    """
    @classmethod
    def run(cls, num_user, num_item, rating_path):
        """
        build user-item matrix

        Args:
            num_user (int): number of users
            num_item (int): number of items(movies)
            rating_path (str): path of the rating information (has userNo itemNo rating timestamp per line)

        Returns:
            numpy.array: user*item matrix with 0-1 value on it
        """
        matrix = np.empty((num_user, num_item))
        try:
            # open file
            f = open(rating_path, 'r')

            for line in f:      # per each line
                parsed_info = line.split()
                if len(parsed_info) >= 3:   # fill matrix value
                    matrix[int(parsed_info[0]), int(parsed_info[1])] = 0.5 if int(parsed_info[2]) >= 4 else -0.5
        finally:
            if f is not None:
                f.close()

        return matrix
