# -*- coding:utf-8 -*-

"""
test.py

run collaborative filtering

"""
from matrix_builder import BuildMatrix
from cbf import CollaborativeFiltering


class TestCollaborativeFiltering(object):
    """ test collaborative filtering
    """
    @classmethod
    def run(cls, condition_path, train_path, test_path):
        """
        run collaborative filtering

        Args:
            train_path (str): user*item matrix with given ratings. has -0.5, +0.5 depends on their preferences
            test_path (str): testcase by 3 matrix that want to guess
        """

        try:
            # open file
            f = open(condition_path, 'r')
            num_user = int(f.readline().split()[0])
            num_item = int(f.readline().split()[0])
        finally:
            if f is not None:
                f.close()

        train_matrix = BuildMatrix.run(num_user+1, num_item+1, train_path)

        uu_list = []
        ii_list = []
        try:
            # open file
            f = open(test_path, 'r')

            for line in f:      # per each line
                parsed_info = line.split()
                if len(parsed_info) >= 3:   # fill matrix value
                    uu_list.append([int(parsed_info[0]), int(parsed_info[1]),
                                    0.5 if int(parsed_info[2]) >= 4 else -0.5])
                    ii_list.append([int(parsed_info[1]), int(parsed_info[0]),
                                    0.5 if int(parsed_info[2]) >= 4 else -0.5])
        finally:
            if f is not None:
                f.close()

        print 'test user-user error : %f' % CollaborativeFiltering.run(train_matrix, uu_list)
        print 'test item-item error : %f' % CollaborativeFiltering.run(train_matrix.T, ii_list)

for i in range(1,5):
    base_path = 'ml-100k/u%d.base' % i
    test_path = 'ml-100k/u%d.test' % i
    TestCollaborativeFiltering.run('ml-100k/u.info', base_path, test_path)
