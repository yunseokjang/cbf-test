# -*- coding:utf-8 -*-

"""
cos_similarity.py

compute cosine similarity of row vectors

"""
import numpy as np


class ComputeCosineSimilarity(object):
    """ compute cosine similarity of row vectors in numpy matrix
    """
    @classmethod
    def run_zero_min(cls, original_matrix):
        """
        compute zero-min cosine similarity

        Args:
            matrix (numpy.array): m*n matrix that want to compute

        Returns:
            numpy.array: mean matrix
            numpy.array: m*m symmetric cosine similarity matrix
        """
        # make zero mean & centered
        mean_matrix = original_matrix.sum(1).astype(float) / (original_matrix != 0).sum(1)
        mean_matrix[np.isinf(mean_matrix)] = 0
        mean_matrix[np.isnan(mean_matrix)] = 0
        matrix = original_matrix - np.multiply((original_matrix != 0).astype(int), mean_matrix[:, np.newaxis])

        return mean_matrix, cls.run(matrix)


    @classmethod
    def run(cls, matrix):
        """
        compute cosine similarity

        Args:
            matrix (numpy.array): m*n matrix that want to compute

        Returns:
            numpy.array: m*m symmetric cosine similarity matrix
        """

        # base similarity matrix (all dot products)
        # replace this with A.dot(A.T).todense() for sparse representation
        similarity = np.dot(matrix, matrix.T)

        # squared magnitude of preference vectors (number of occurrences)
        square_mag = np.diag(similarity)

        # inverse squared magnitude
        inv_square_mag = 1 / square_mag

        # if it doesn't occur, set it's inverse magnitude to zero (instead of inf)
        inv_square_mag[np.isinf(inv_square_mag)] = 0

        # inverse of the magnitude
        inv_mag = np.sqrt(inv_square_mag)

        # cosine similarity (elementwise multiply by inverse magnitudes)
        cosine = similarity * inv_mag
        return cosine.T * inv_mag
